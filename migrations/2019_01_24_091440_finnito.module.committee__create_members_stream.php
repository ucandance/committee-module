<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class FinnitoModuleCommitteeCreateMembersStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'members',
        'title_column' => 'name',
        "namespace" => "repeater",
        'versionable' => true,
        'searchable' => false,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'name' => [
            'required' => true,
        ],
        "role",
        "image",
        "description",
    ];

}
