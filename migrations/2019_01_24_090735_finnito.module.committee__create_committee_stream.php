<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class FinnitoModuleCommitteeCreateCommitteeStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'committee',
        'title_column' => 'year',
        'versionable' => false,
        'searchable' => false,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'year' => [
            'unique' => true,
            'required' => true,
        ],
        'members' => [],
        "meta_image",
        "meta_description",
    ];

}
