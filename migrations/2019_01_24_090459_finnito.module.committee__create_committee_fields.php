<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;
use Finnito\CommitteeModule\Member\MemberModel;

class FinnitoModuleCommitteeCreateCommitteeFields extends Migration
{

    /**
     * The addon fields.
     *
     * @var array
     */
    protected $fields = [
        "year" => [
            "type" => "anomaly.field_type.integer",
            "config" => [
                "separator" => null,
                "min" => 1993
            ],
        ],
        "members" => [
            "type" => "anomaly.field_type.repeater",
            "config" => [
                "related" => MemberModel::class,
                "add_row" => "Add Member",
            ],
        ],
        "meta_image" => [
            "type" => "anomaly.field_type.file",
            "config" => [
                "folders" => ["committee_images"],
                "max" => 1,
            ],
        ],
        "meta_description" => [
            "type" => "anomaly.field_type.textarea",
            "config" => [
                "show_counter" => true,
                "max" => "160",
            ],
        ],

        // Repeater Fields
        "name" => [
            "type" => "anomaly.field_type.text",
            "namespace" => "repeater",
            "config" => [],
        ],
        "role" => [
            "type" => "anomaly.field_type.text",
            "namespace" => "repeater",
            "config" => [],
        ],
        "description" => [
            "type" => "anomaly.field_type.textarea",
            "namespace" => "repeater",
            "config" => [
                "show_counter" => true,
            ],
        ],
        "image" => [
            "type" => "anomaly.field_type.file",
            "namespace" => "repeater",
            "config" => [
                "folders" => ["committee_images"],
                "max" => 1,
            ],
        ],
    ];

}
