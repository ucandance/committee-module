<?php

use Anomaly\PagesModule\Page\PageModel;

return [
    "parent_page" => [
        "type" => "anomaly.field_type.relationship",
        "config" => [
            "related" => PageModel::class,
        ],
    ],
];
