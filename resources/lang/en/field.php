<?php

return [
    "year" => [
        "name" => "Committee Year",
    ],
    "members" => [
        "name" => "Committee Members"
    ],
    "name" => [
        "name" => "Full Name",
    ],
    "role" => [
        "name" => "Committee Role",
        "instructions" => "E.g. President, Vice President, Promotions Officer",
    ],
    "image" => [
        "name" => "Profile Image",
    ],
    "description" => [
        "name" => "Description",
    ],
    "meta_image" => [
        "name" => "Meta Image",
        "instructions" => "Displayed when the link is shared in social media."
    ],
    "meta_description" => [
        "name" => "Meta Description",
        "instructions" => "Displayed when the link is shared in social media.", 
    ],
];
