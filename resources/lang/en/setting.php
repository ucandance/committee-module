<?php

return [
    "parent_page" => [
        "name" => "Parent Page",
        "instructions" => "The page that the committee pages are a descendent of. Must be top-level."
    ],
];
