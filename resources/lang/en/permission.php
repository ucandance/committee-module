<?php

return [
    'committee' => [
        'name'   => 'Committee',
        'option' => [
            'read'   => 'Can read committee?',
            'write'  => 'Can create/edit committee?',
            'delete' => 'Can delete committee?',
        ],
    ],
    'members' => [
        'name'   => 'Members',
        'option' => [
            'read'   => 'Can read members?',
            'write'  => 'Can create/edit members?',
            'delete' => 'Can delete members?',
        ],
    ],
    'members' => [
        'name'   => 'Members',
        'option' => [
            'read'   => 'Can read members?',
            'write'  => 'Can create/edit members?',
            'delete' => 'Can delete members?',
        ],
    ],
];
