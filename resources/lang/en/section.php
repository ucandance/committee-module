<?php

return [
    'committee' => [
        'title' => 'Committee',
    ],
    'members' => [
        'title' => 'Members',
    ],
    'configuration' => [
        'title' => 'Configuration',
    ],
];
