<?php namespace Finnito\CommitteeModule\Committee\Form;

use Anomaly\Streams\Platform\Ui\Form\FormBuilder;

class CommitteeFormBuilder extends FormBuilder
{

    /**
     * The form fields.
     *
     * @var array|string
     */
    protected $fields = [];

    /**
     * Additional validation rules.
     *
     * @var array|string
     */
    protected $rules = [];

    /**
     * Fields to skip.
     *
     * @var array|string
     */
    protected $skips = [];

    /**
     * The form actions.
     *
     * @var array|string
     */
    protected $actions = [];

    /**
     * The form buttons.
     *
     * @var array|string
     */
    protected $buttons = [
        'cancel',
    ];

    /**
     * The form options.
     *
     * @var array
     */
    protected $options = [];

    /**
     * The form sections.
     *
     * @var array
     */
    protected $sections = [
        "main" => [
            "tabs" => [
                "committee" => [
                    'title'  => 'finnito.module.committee::tab.committee',
                    "fields" => [
                        "year",
                        "members",
                    ]
                ],
                "seo" => [
                    'title'  => 'finnito.module.committee::tab.seo',
                    "fields" => [
                        "meta_description",
                        "meta_image",
                    ],
                ]

            ],
        ],
    ];

    /**
     * The form assets.
     *
     * @var array
     */
    protected $assets = [];

}
