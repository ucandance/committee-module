<?php namespace Finnito\CommitteeModule\Committee\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface CommitteeRepositoryInterface extends EntryRepositoryInterface
{

}
