<?php namespace Finnito\CommitteeModule\Committee;

use Finnito\CommitteeModule\Committee\Contract\CommitteeRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class CommitteeRepository extends EntryRepository implements CommitteeRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var CommitteeModel
     */
    protected $model;

    /**
     * Create a new CommitteeRepository instance.
     *
     * @param CommitteeModel $model
     */
    public function __construct(CommitteeModel $model)
    {
        $this->model = $model;
    }
}
