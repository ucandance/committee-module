<?php namespace Finnito\CommitteeModule\Committee;

use Finnito\CommitteeModule\Committee\Contract\CommitteeInterface;
use Anomaly\Streams\Platform\Model\Committee\CommitteeCommitteeEntryModel;

class CommitteeModel extends CommitteeCommitteeEntryModel implements CommitteeInterface
{
}
