<?php namespace Finnito\CommitteeModule;

use Anomaly\Streams\Platform\Addon\AddonServiceProvider;
use Finnito\CommitteeModule\Member\Contract\MemberRepositoryInterface;
use Finnito\CommitteeModule\Member\MemberRepository;
use Anomaly\Streams\Platform\Model\Committee\CommitteeMembersEntryModel;
use Finnito\CommitteeModule\Member\MemberModel;
use Finnito\CommitteeModule\Committee\Contract\CommitteeRepositoryInterface;
use Finnito\CommitteeModule\Committee\CommitteeRepository;
use Anomaly\Streams\Platform\Model\Committee\CommitteeCommitteeEntryModel;
use Finnito\CommitteeModule\Committee\CommitteeModel;
use Illuminate\Routing\Router;
use Anomaly\SettingsModule\Setting\Contract\SettingRepositoryInterface;
use Anomaly\PagesModule\Page\Contract\PageRepositoryInterface;

class CommitteeModuleServiceProvider extends AddonServiceProvider
{

    /**
     * Additional addon plugins.
     *
     * @type array|null
     */
    protected $plugins = [];

    /**
     * The addon Artisan commands.
     *
     * @type array|null
     */
    protected $commands = [];

    /**
     * The addon's scheduled commands.
     *
     * @type array|null
     */
    protected $schedules = [];

    /**
     * The addon API routes.
     *
     * @type array|null
     */
    protected $api = [];

    /**
     * The addon routes.
     *
     * @type array|null
     */
    protected $routes = [
        "admin/committee/configuration" => "Finnito\CommitteeModule\Http\Controller\Admin\ConfigurationController@index",
        'admin/committee/members'           => 'Finnito\CommitteeModule\Http\Controller\Admin\MembersController@index',
        'admin/committee/members/create'    => 'Finnito\CommitteeModule\Http\Controller\Admin\MembersController@create',
        'admin/committee/members/edit/{id}' => 'Finnito\CommitteeModule\Http\Controller\Admin\MembersController@edit',
        'admin/committee/members'           => 'Finnito\CommitteeModule\Http\Controller\Admin\MembersController@index',
        'admin/committee/members/create'    => 'Finnito\CommitteeModule\Http\Controller\Admin\MembersController@create',
        'admin/committee/members/edit/{id}' => 'Finnito\CommitteeModule\Http\Controller\Admin\MembersController@edit',
        'admin/committee'           => 'Finnito\CommitteeModule\Http\Controller\Admin\CommitteeController@index',
        'admin/committee/create'    => 'Finnito\CommitteeModule\Http\Controller\Admin\CommitteeController@create',
        'admin/committee/edit/{id}' => 'Finnito\CommitteeModule\Http\Controller\Admin\CommitteeController@edit',
    ];

    /**
     * The addon middleware.
     *
     * @type array|null
     */
    protected $middleware = [
        //Finnito\CommitteeModule\Http\Middleware\ExampleMiddleware::class
    ];

    /**
     * Addon group middleware.
     *
     * @var array
     */
    protected $groupMiddleware = [
        //'web' => [
        //    Finnito\CommitteeModule\Http\Middleware\ExampleMiddleware::class,
        //],
    ];

    /**
     * Addon route middleware.
     *
     * @type array|null
     */
    protected $routeMiddleware = [];

    /**
     * The addon event listeners.
     *
     * @type array|null
     */
    protected $listeners = [
        //Finnito\CommitteeModule\Event\ExampleEvent::class => [
        //    Finnito\CommitteeModule\Listener\ExampleListener::class,
        //],
    ];

    /**
     * The addon alias bindings.
     *
     * @type array|null
     */
    protected $aliases = [
        //'Example' => Finnito\CommitteeModule\Example::class
    ];

    /**
     * The addon class bindings.
     *
     * @type array|null
     */
    protected $bindings = [
        CommitteeMembersEntryModel::class => MemberModel::class,
        CommitteeMembersEntryModel::class => MemberModel::class,
        CommitteeCommitteeEntryModel::class => CommitteeModel::class,
    ];

    /**
     * The addon singleton bindings.
     *
     * @type array|null
     */
    protected $singletons = [
        MemberRepositoryInterface::class => MemberRepository::class,
        MemberRepositoryInterface::class => MemberRepository::class,
        CommitteeRepositoryInterface::class => CommitteeRepository::class,
    ];

    /**
     * Additional service providers.
     *
     * @type array|null
     */
    protected $providers = [
        //\ExamplePackage\Provider\ExampleProvider::class
    ];

    /**
     * The addon view overrides.
     *
     * @type array|null
     */
    protected $overrides = [
        //'streams::errors/404' => 'module::errors/404',
        //'streams::errors/500' => 'module::errors/500',
    ];

    /**
     * The addon mobile-only view overrides.
     *
     * @type array|null
     */
    protected $mobile = [
        //'streams::errors/404' => 'module::mobile/errors/404',
        //'streams::errors/500' => 'module::mobile/errors/500',
    ];

    /**
     * Register the addon.
     */
    public function register()
    {
        // Run extra pre-boot registration logic here.
        // Use method injection or commands to bring in services.
    }

    /**
     * Boot the addon.
     */
    public function boot()
    {
        // Run extra post-boot registration logic here.
        // Use method injection or commands to bring in services.
    }

    /**
     * Map additional addon routes.
     *
     * @param Router $router
     */
    public function map(
        Router $router,
        SettingRepositoryInterface $settings,
        PageRepositoryInterface $pages
    ) {
        // Register dynamic routes here for example.
        // Use method injection or commands to bring in services.
        $parentPageID = $settings->value("finnito.module.committee::parent_page");
        if ($parentPage = $pages->find($parentPageID)) {
            $router->get(
                "{$parentPage->slug}/{year}",
                ['uses' => 'Finnito\CommitteeModule\Http\Controller\CommitteeController@year',]
            );
        }
    }

}
