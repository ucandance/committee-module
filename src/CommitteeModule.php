<?php namespace Finnito\CommitteeModule;

use Anomaly\Streams\Platform\Addon\Module\Module;

class CommitteeModule extends Module
{

    /**
     * The navigation display flag.
     *
     * @var bool
     */
    protected $navigation = true;

    /**
     * The addon icon.
     *
     * @var string
     */
    protected $icon = 'users';

    /**
     * The module sections.
     *
     * @var array
     */
    protected $sections = [
        'committee' => [
            'buttons' => [
                'new_committee',
            ],
        ],
        'configuration' => [],
    ];

}
