<?php namespace Finnito\CommitteeModule\Http\Controller\Admin;

// use Finnito\CommitteeModule\Committee\Form\CommitteeFormBuilder;
// use Finnito\CommitteeModule\Committee\Table\CommitteeTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;
use Anomaly\SettingsModule\Setting\Form\SettingFormBuilder;

class ConfigurationController extends AdminController
{

    /**
     * Display a settings form.
     *
     * @param CommitteeTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(SettingFormBuilder $settings)
    {
        return $settings->render("finnito.module.committee");
    }
}
