<?php namespace Finnito\CommitteeModule\Http\Controller\Admin;

use Finnito\CommitteeModule\Committee\Form\CommitteeFormBuilder;
use Finnito\CommitteeModule\Committee\Table\CommitteeTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class CommitteeController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param CommitteeTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(CommitteeTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param CommitteeFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(CommitteeFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param CommitteeFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(CommitteeFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
