<?php namespace Finnito\CommitteeModule\Http\Controller;

use Anomaly\Streams\Platform\Http\Controller\PublicController;
use Finnito\CommitteeModule\Committee\Contract\CommitteeRepositoryInterface;

class CommitteeController extends PublicController
{

    /**
     * Return a committee view
     * for a given year.
     *
     * @param CommitteeRepositoryInterface $committies
     * @param String $year
     * @return \Illuminate\Contracts\View\View|mixed
     */
    public function year(CommitteeRepositoryInterface $committies, $year)
    {
        if (!$committee = $committies->findBy("year", $year)) {
            abort(404);
        }

        // Breadcrumbs
        $this->breadcrumbs->add("Home", "/");
        $this->breadcrumbs->add('About Us', '/about-us');
        $this->breadcrumbs->add("{$year} Committee", "/about-us/{$year}");

        // Meta Information
        $this->template->set("meta_title", "{$year} Committee");
        $this->template->set("meta_description", $committee->meta_description);
        $this->template->set("meta_image", $committee->meta_image->id);

        // Edt Links
        $this->template->set('edit_link', "/admin/committee/edit/".$committee->id);
        $this->template->set("edit_type", "{$year} Committee");

        return $this->view->make(
            "finnito.module.committee::year",
            [
                "year" => $year,
                "committee" => $committee,
                "committies" => $committies->all(),
            ]
        );
    }
}
