<?php namespace Finnito\CommitteeModule\Member;

use Finnito\CommitteeModule\Member\Contract\MemberInterface;
use Anomaly\Streams\Platform\Model\Committee\CommitteeCommitteeEntryModel;
use Anomaly\Streams\Platform\Model\Repeater\RepeaterMembersEntryModel;

class MemberModel extends RepeaterMembersEntryModel implements MemberInterface
{
}
