<?php namespace Finnito\CommitteeModule\Member\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface MemberRepositoryInterface extends EntryRepositoryInterface
{

}
